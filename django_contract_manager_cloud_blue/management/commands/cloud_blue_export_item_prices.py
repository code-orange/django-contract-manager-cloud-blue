import xlrd
from django.core.management.base import BaseCommand

from django_contract_manager_cloud_blue.django_contract_manager_cloud_blue.models import (
    CloudBlueProducts,
)


class Command(BaseCommand):
    def handle(self, *args, **options):
        book = xlrd.open_workbook("data/cloud_blue_product_list.xls")

        # Now to print the number of worksheets in the excel file
        print("The number of worksheets are " + str(book.nsheets))

        # Now the names of the worksheets in excel file
        print(
            "The names of worksheets are" + str(book.sheet_names())
        )  # returns an array of names

        # Choose a specific workbook to import data
        sheet = book.sheet_by_index(0)

        # skip first line
        for row_count in range(1, sheet.nrows):
            cb_item_code = sheet.cell_value(row_count, 0)
            cb_item_price = sheet.cell_value(row_count, 11)

            try:
                cb_item = CloudBlueProducts.objects.get(cb_item_code=cb_item_code)
            except CloudBlueProducts.DoesNotExist:
                continue

            if not cb_item.item.external_id.startswith("MP-C"):
                continue

            print(cb_item.item.external_id + ";" + str(cb_item_price))
