from pprint import pprint

from django.core.management.base import BaseCommand

from django_contract_manager_cloud_blue.django_contract_manager_cloud_blue.func import *


class Command(BaseCommand):
    def handle(self, *args, **options):
        token = cb_fetch_token()
        pprint(token)

        customers = cb_customers_list(token)
        pprint(customers)

        products = cb_products_list(token)
        pprint(products)
