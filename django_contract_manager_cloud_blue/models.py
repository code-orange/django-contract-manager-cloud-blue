from django_mdat_customer.django_mdat_customer.models import *
from datetime import datetime


class CloudBlueProducts(models.Model):
    cb_item_code = models.CharField(
        max_length=250, null=False, blank=False, unique=True
    )
    cb_item_name = models.CharField(
        max_length=250, null=False, blank=False, unique=False
    )
    item = models.ForeignKey(MdatItems, models.DO_NOTHING, null=True, unique=True)
    date_added = models.DateTimeField(default=datetime.now)

    class Meta:
        db_table = "cloud_blue_products"


class CloudBlueCustomerMapping(models.Model):
    customer = models.ForeignKey(MdatCustomers, models.DO_NOTHING, unique=True)
    cloud_blue_id = models.BigIntegerField(null=True, unique=True)
    date_added = models.DateTimeField(default=datetime.now)

    class Meta:
        db_table = "cloud_blue_customer_mapping"
