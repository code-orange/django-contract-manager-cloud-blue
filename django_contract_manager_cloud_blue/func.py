import requests
from django.conf import settings
from requests.auth import HTTPBasicAuth


def cb_fetch_token():
    response = requests.post(
        settings.CLOUDBLUE_MARKETPLACE_API_URL + "/token",
        auth=HTTPBasicAuth(
            settings.CLOUDBLUE_MARKETPLACE_API_USER,
            settings.CLOUDBLUE_MARKETPLACE_API_PASSWD,
        ),
        headers={
            "Content-Type": "application/json",
            "X-Subscription-Key": settings.CLOUDBLUE_MARKETPLACE_API_KEY,
        },
        json={
            "marketplace": "de",
        },
    )

    return response.json()["token"]


def cb_get_data(token: str, endpoint: str, params=None):
    if params is None:
        params = dict()

    response = requests.get(
        settings.CLOUDBLUE_MARKETPLACE_API_URL + endpoint,
        headers={
            "Authorization": "Bearer " + token,
            "Content-Type": "application/json",
            "X-Subscription-Key": settings.CLOUDBLUE_MARKETPLACE_API_KEY,
        },
        params=params,
    )

    return response.json()


def cb_post_data(token: str, endpoint: str, params=None, data=None):
    if params is None:
        params = dict()

    if data is None:
        data = dict()

    response = requests.post(
        settings.CLOUDBLUE_MARKETPLACE_API_URL + endpoint,
        headers={
            "Authorization": "Bearer " + token,
            "Content-Type": "application/json",
            "X-Subscription-Key": settings.CLOUDBLUE_MARKETPLACE_API_KEY,
        },
        params=params,
        json=data,
    )

    return response.json()


def cb_customers_list(token: str):
    customers = cb_get_data(
        token,
        "/customers",
        params={
            "limit": 100000,
        },
    )

    return customers["data"]


def cb_customers_create(token: str, data: dict):
    new_customer = cb_post_data(
        token,
        "/customers",
        data=data,
    )

    return new_customer


def cb_products_list(token: str):
    products = cb_get_data(
        token,
        "/products",
        params={
            "limit": 100000,
        },
    )

    return products["data"]


def cb_orders_create(token: str, data: dict):
    new_order = cb_post_data(
        token,
        "/orders",
        data=data,
    )

    return new_order


def cb_orders_list(token: str):
    orders = cb_get_data(
        token,
        "/orders",
        params={
            "limit": 100000,
        },
    )

    return orders["data"]


def cb_orders_detail(token: str, order_id: int):
    order_detail = cb_get_data(
        token,
        "/orders/" + str(order_id),
        params={
            "limit": 100000,
        },
    )

    return order_detail


def cb_orders_estimate(token: str, data: dict):
    new_order = cb_post_data(
        token,
        "/orders/estimate",
        data=data,
    )

    return new_order
