# Generated by Django 3.2.13 on 2022-04-27 12:57

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("django_contract_manager_cloud_blue", "0007_make_item_id_unique"),
    ]

    operations = [
        migrations.AlterField(
            model_name="cloudbluecustomermapping",
            name="id",
            field=models.BigAutoField(
                auto_created=True, primary_key=True, serialize=False, verbose_name="ID"
            ),
        ),
        migrations.AlterField(
            model_name="cloudblueproducts",
            name="id",
            field=models.BigAutoField(
                auto_created=True, primary_key=True, serialize=False, verbose_name="ID"
            ),
        ),
    ]
