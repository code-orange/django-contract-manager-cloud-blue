# Generated by Django 3.1.13 on 2021-08-27 15:39

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("django_contract_manager_cloud_blue", "0003_add_cloud_blue_customer_mapping"),
    ]

    operations = [
        migrations.AlterField(
            model_name="cloudbluecustomermapping",
            name="cloud_blue_id",
            field=models.BigIntegerField(null=True),
        ),
    ]
